console.log("Hello World!");


// ARRAY METHODS
	// similar to functions, theses array methods can be done in array and objects alone.

/*
	MUTATOR Methods
		
*/
let fruits = ['Apple', 'Mango', 'Rambutan', 'Lanzones', 'Durian'];

console.log(fruits);

// push()
/*
		adds an element at the end of the array

		SYNTAX:
			arrayName.push(newElement);
*/
fruits.push("Orange");
// it can also accept multiple arguments
fruits.push('Guava','Avocado');
console.log("Mutated Array from Push Method: ")
console.log(fruits);


// pop()
/*
		remove the LAST element of an array

		SYNTAX:
			arrayName.pop()
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from Pop Method: ")
console.log(fruits);

fruits.pop();
console.log("Mutated Array from Pop Method: ")
console.log(fruits);

fruits.pop();
console.log("Mutated Array from Pop Method: ")
console.log(fruits);


// unshift()
/*
	add elements at the start of an array
	similar to push, but from the start of the array

	SYNTAX:
		arrayName.unshift(elementName);

*/
fruits.unshift("Lime","Banana");	
console.log("Mutated Array from Unshift Method: ")
console.log(fruits);


// shift()
/*
	removes the 1st element of an array

	SYNTAX:
		arrayName.shift();
*/
let fruitRemoved = fruits.shift();
console.log("Mutated Array from Shift Method: ")
console.log(fruitRemoved);
console.log(fruits);


// splice()
/*
	simultaneously removes and adds elements starting from the specified index number.

	SYNTAX:
		arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);

	elementsToBeAdded can be MULTIPLE elements, separated by comma.
*/

fruits.splice(1,2, "Lime", "Cherry");
console.log("Mutated Array from Splice Method: ")
console.log(fruits);


// sort()
/*
	rearranges the elements in an array in Alphanumeric order

	SYNTAX:
		arrayName.sort();
*/

fruits.sort();
console.log("Mutated Array from Sort Method: ")
console.log(fruits);

// reverse()
/*
	reverses the order of the array

	SYNTAX:
		arrayName.reverse();

*/

fruits.reverse();
console.log("Mutated Array from Reverse Method: ")
console.log(fruits);




// NON MUTATOR Methods
/*
	- functions that do not modify the original array
	- these methods do not manipulate the elements inside the array even if they are performing tasks such as returning elements from an array and combining them with other arrays and printing the output.
*/


// indexOf() 
/*
	returns the index number of the FIRST matching element found in the array.

	checks from FIRST to LAST
	SYNTAX:
		arrayName.indexOf(searchValue);

*/
let countries = ['PH', 'RUS', 'CHN', 'KOR', 'AU', 'USA', 'CAN', 'JPN', 'PH'];
console.log(countries);

let firstIndex = countries.indexOf('PH')
console.log("Result of indexOf: " + firstIndex);

// searching for non-existing elements will return -1.
firstIndex = countries.indexOf('KAZ')
console.log("Result of indexOf: " + firstIndex);


// lastIndexOf()
/*
	returns the index number of the first matching element in the array STARTING from the LAST element

	checks from LAST to FIRST
	SYNTAX:
		arrayName.lastIndexOf(searchValue);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log("Results of lastIndexOf: " + lastIndex);


// slice()
/*
	copies a part of the array and returns a new array starting from the elements of the specified index number, and ends with the second specified index number (if any)

	SYNTAX:
		arrayName.slice(startingIndex); - from startingIndex to END
		arrayName.slice(startingIndex, endingElement); - from startingIndex to endingElement
*/

// slices the array from index 2 to the end.
let slicedArrayA = countries.slice(2);
console.log("Results of Slice: " + slicedArrayA);
console.log(slicedArrayA);

// slives the array from index 4 to the 7th element
let slicedArrayB = countries.slice(4,7);
console.log("Results of Slice: " + slicedArrayB);
console.log(slicedArrayB);

// slices the array from the last element.
let slicedArrayC = countries.slice(-3);
console.log("Results of Slice: " + slicedArrayC);
console.log(slicedArrayC);


// toString()
/*
	returns the contents of the array as a String.

	SYNTAX:
		let newVar = arrayName.toString();
*/
let stringArray = countries.toString();
console.log("Results of toString: ");
console.log(stringArray);

// concat()
/*
	used to combine two or more arrays and returns the resulting array

	SYNTAX:
		arrayA.concat(arrayB);
	
*/

let tasksA = ['drink HTML', 'eat JavaScript'];
let tasksB = ['inhale CSS', 'breathe SASS'];
let tasksC = ['get GIT', 'be Node'];

let tasks = tasksA.concat(tasksB);
console.log("Results of concat: ");
console.log(tasks);


//  combining multiple arrays

let allTasks = tasksA.concat(tasksB, tasksC);
console.log(allTasks);



// join()
/*
	returns the array as a string with connectors depeding on the specified separator:

	SYNTAX:
		arrayName.join()

		join() - connects with comma
		join(' ') - connects elements with space
		join('-') - connects elements with -
*/

let users = ['John', 'Jane', 'Joe', 'Jobert', 'Julius'];

console.log(users.join());
console.log(users.join(' '));
console.log(users.join('-'));




// ITERATION Methods
/*
	- iteration methods are loops designed to perform repetitive tasks on arrays.
	- useful for manipulating array data resulting in complex tasks.	
*/

// forEach
/*
	- similar to a for loop that loops through all elements
	- variable names for arrays usually written in plural form of the data stored in an array
	- it's common practice to use singular form of the array content for parameter names used in array names.
	- array iterations normally work with a fxn supplied as an argument
	- how these fxn works is by performing tasks that are predefined within the array's method.

	SYNTAX:
		arrayName.forEach(funtion(individualElement){
			statements;
		})
*/
allTasks.forEach(function(task){
	console.log(task);
})


// forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if (task.length > 10) {
	// we stored the filtered elements inside another var to avoid confusions should we need the original array intact.
		filteredTasks.push(task);
	}
})

console.log("Results of forEach: ");
console.log(filteredTasks);




// map()
/*
	- iterates on each element AND returns a new array with different values depending on the result of the fxn's operation.
	- this is also useful in performing tasks where mutating/changing the elements are required.

	SYNTAX:
		let resultingArray = arrayName.map(function(individualElement){
			return statement;
		});
*/

let numbers = [1,2,3,4,5];
console.log(numbers);

let numbersMap = numbers.map(function(number){
	// unlike forEach, return statement is needed in map method to create a value that is stored in the new array.
	return number*number;
});
console.log("Results of map: ");
console.log(numbersMap);





// every()
/*
	(similar to AND operator)
	- checks if all elements in an array passes the given condition.
	- returns a boolean data type depending if all elements meet the condition or not.

	SYNTAX:
		let/const result = arrayName.every(function(individualElement){
			return expression/condition;
		})
*/
let allValid = numbers.every(function(number){
	return (number < 3);
})
console.log("Results of every: ");
console.log(allValid);



// some()
/*
	(similar to OR operator)
	- checks if at least ONE element in an array passes the given condition.

	SYNTAX:
		let/const result = arrayName.some(function(individualElement){
			return expression/condition;
		})
*/

let someValid = numbers.some(function(number){
	return (number < 2);
})
console.log("Results of some: ");
console.log(someValid);



// filter()
/*
	- returns a new array that contains elements which passes the given condition.
	- returns an empty array if there are no elements that meet the condition.
	- useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods such as (forEach + if statements + push) in an earlier example.

		*as devs, it is important that we make our work as efficient as possible.

	SYNTAX:
		let/const resultName = arrayName.filter(function(individualElement){
			return expression/condition;
		})
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Results of filter: ");
console.log(filterValid);



let parts = ['Mouse', 'Keyboard', 'Unit', 'Monitor'];
console.log(parts);

// reduce()
/*
	evaluates elements from left to right and returns/reduces the array into a single value

	SYNTAX:
		let/const resultName = arrayName.reduce(function(accumulator,currentValue))
		accumulator - stores the result for every iteration
		currentValue - is the next/current element in the array that is evaluated in each iteration
*/


let iteration = 0;
let reduceArray = numbers.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x + y; 
})

console.log(reduceArray);

// when used in string, it returns the concatinated string of the array
let reduceStringArray = parts.reduce(function(x,y){
	console.warn(iteration);
	console.log(x);
	console.log(y);
	return x + y; 
});
console.log(reduceStringArray);


// include method
let filteredParts = parts.filter(function(part){
	return part.toLowerCase().includes("unit");
})

console.log(filteredParts);
